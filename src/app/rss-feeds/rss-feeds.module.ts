import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RssFeedsPageRoutingModule } from './rss-feeds-routing.module';
import { FeedItemsPage } from './feed-items/feed-items.page';
import { SharedModule } from '../shared/shared.module';
import { ManageFeedSourcePage } from './manage-feed-source/manage-feed-source.page';
import { FeedModalPage } from './manage-feed-source/feed-modal/feed-modal.page';
import { SavedFeedItemsPage } from './saved-feed-items/saved-feed-items.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RssFeedsPageRoutingModule,
    SharedModule
  ],
  declarations: [
    FeedItemsPage,
    ManageFeedSourcePage,
    FeedModalPage,
    SavedFeedItemsPage
  ]
})
export class RssFeedsPageModule {}
