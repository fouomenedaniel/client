import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { SavedFeedItem } from '../../model/saved-feed-item';
import { RssFeedsApi } from '../rss-feeds.api';
import { TokenStorageService } from '../../shared/token-storage.service';
import { tap } from 'rxjs/operators';
import { Page } from '../../model/page';
import { PageRequest } from '../../model/pageRequest';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-saved-feed-items',
  templateUrl: './saved-feed-items.page.html',
  styleUrls: ['./saved-feed-items.page.scss'],
})
export class SavedFeedItemsPage implements OnInit, OnDestroy {

  savedFeedItems$: Observable<Page<SavedFeedItem>>;
  userId: string;
  subscriptions: Subscription[] = [];
  page = 0;
  pageSize = 10;

  constructor(private readonly rssFeedsApi: RssFeedsApi,
              private readonly tokenStorageService: TokenStorageService,
              private readonly toastController: ToastController) { }

  ngOnInit() {
    this.subscriptions.push(this.tokenStorageService.getId().pipe(
      tap(userId => {
        if (userId) {
          this.userId = userId;
          this.loadUserSavedFeedItems();
        }
      })
    ).subscribe());
  }

  ionViewWillEnter() {
    this.loadUserSavedFeedItems();
  }

  onDeleteItem(itemId: number) {
    this.subscriptions.push(
      this.rssFeedsApi.deleteSavedFeedItem(itemId).pipe(
        tap(_ => {
          this.presentToast('Article supprimé', 'success');
          this.loadUserSavedFeedItems();
        })
      ).subscribe()
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  private loadUserSavedFeedItems() {
    const pageRequest: PageRequest = { page : this.page, size: this.pageSize};
    this.savedFeedItems$ = this.rssFeedsApi.getUserSavedFeedItems(this.userId, pageRequest);
  }

  private async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color
    });
    await toast.present();
  }
}
