import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SavedFeedItemsPage } from './saved-feed-items.page';

describe('SavedFeedItemsPage', () => {
  let component: SavedFeedItemsPage;
  let fixture: ComponentFixture<SavedFeedItemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedFeedItemsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SavedFeedItemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
