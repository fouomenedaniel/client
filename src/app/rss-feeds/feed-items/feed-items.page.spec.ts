import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FeedItemsPage } from './feed-items.page';

describe('FeedItemsPage', () => {
  let component: FeedItemsPage;
  let fixture: ComponentFixture<FeedItemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedItemsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FeedItemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
