import { Injectable } from '@angular/core';
import { RSS_FEEDS_API_URL } from '../constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RssFeed } from '../model/rss-feed';
import { createRequestOption } from '../shared/requestUtils';
import { FeedItem } from '../model/feed-item';
import { PageRequest } from '../model/pageRequest';
import { SavedFeedItem } from '../model/saved-feed-item';
import { Page } from '../model/page';

@Injectable({
  providedIn: 'root'
})
export class RssFeedsApi {
  rootUrl = `${RSS_FEEDS_API_URL}/feeds`;
  userFeedsUrl = `${this.rootUrl}/users`;
  savedFeedsItemsUrl = `${this.rootUrl}/saved`;

  constructor(protected readonly http: HttpClient) {}

  getFeedsSources(userId?: string): Observable<RssFeed[]> {
    let options = null;
    if (userId) {
      options = createRequestOption({userId});
    }
    return this.http.get<RssFeed[]>(this.rootUrl, {params: options});
  }

  readFeed(feedId: number, userId: string): Observable<FeedItem[]> {
    const options = createRequestOption({userId});
    return this.http.get<FeedItem[]>(`${this.rootUrl}/${feedId}/news`, {params: options});
  }

  getUserSavedFeedItems(userId: string, page?: PageRequest): Observable<Page<SavedFeedItem>> {
    const options = createRequestOption({
      userId,
      page: page?.page,
      size: page?.size,
      direction: page?.direction,
      unpaged: page?.unpaged
    });
    return this.http.get<Page<SavedFeedItem>>(this.savedFeedsItemsUrl, {params: options});
  }

  saveFeedItem(savedFeedItem: SavedFeedItem): Observable<HttpResponse<string>> {
    return this.http.post<string>(this.savedFeedsItemsUrl, savedFeedItem, {observe: 'response'});
  }

  deleteSavedFeedItem(id: number): Observable<HttpResponse<string>> {
    return this.http.delete<string>(`${this.savedFeedsItemsUrl}/${id}`, {observe: 'response'});
  }

  getAllUserRssFeeds(userId: string): Observable<RssFeed[]> {
    const options = createRequestOption({userId});
    return this.http.get<RssFeed[]>(this.userFeedsUrl, {params: options});
  }

  getUserRssFeed(id: number): Observable<RssFeed> {
    return this.http.get<RssFeed>(`${this.userFeedsUrl}/${id}`);
  }

  createOrUpdateRssFeed(rssFeed: RssFeed): Observable<HttpResponse<string>> {
    if (rssFeed.id) {
      return this.updateRssFeed(rssFeed);
    }
    return this.addRssFeed(rssFeed);
  }

  deleteRssFeed(id: number): Observable<HttpResponse<void>> {
    return this.http.delete<void>(`${this.userFeedsUrl}/${id}`, {observe: 'response'});
  }

  private addRssFeed(rssFeed: RssFeed): Observable<HttpResponse<string>> {
    return this.http.post<string>(this.userFeedsUrl, rssFeed, {observe: 'response'});
  }

  private updateRssFeed(rssFeed: RssFeed): Observable<HttpResponse<string>> {
    return this.http.put<string>(`${this.userFeedsUrl}/${rssFeed.id}`, rssFeed, {observe: 'response'});
  }
}
