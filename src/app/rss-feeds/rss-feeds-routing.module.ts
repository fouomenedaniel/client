import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeedItemsPage } from './feed-items/feed-items.page';
import { ManageFeedSourcePage } from './manage-feed-source/manage-feed-source.page';
import { SavedFeedItemsPage } from './saved-feed-items/saved-feed-items.page';

const routes: Routes = [
  {
    path: 'feed-items',
    component: FeedItemsPage
  },
  {
    path: 'feed-sources',
    component: ManageFeedSourcePage
  },
  {
    path: 'saved-feed-items',
    component: SavedFeedItemsPage
  },
  {
    path: '',
    component: ManageFeedSourcePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RssFeedsPageRoutingModule {}
