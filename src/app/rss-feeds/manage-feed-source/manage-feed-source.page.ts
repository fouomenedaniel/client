import { Component, OnDestroy, OnInit } from '@angular/core';
import { RssFeedsApi } from '../rss-feeds.api';
import { TokenStorageService } from '../../shared/token-storage.service';
import { Observable, of, Subscription } from 'rxjs';
import { RssFeed } from '../../model/rss-feed';
import { catchError, finalize, tap } from 'rxjs/operators';
import { ModalController, ToastController } from '@ionic/angular';
import { FeedModalPage } from './feed-modal/feed-modal.page';

@Component({
  selector: 'app-manage-feed-source',
  templateUrl: './manage-feed-source.page.html',
  styleUrls: ['./manage-feed-source.page.scss'],
})
export class ManageFeedSourcePage implements OnInit, OnDestroy {

  rssFeeds$: Observable<RssFeed[]>;
  subscriptions: Subscription[];
  userId: string;
  isLoading = false;

  constructor(private readonly rssFeedsApi: RssFeedsApi,
              private readonly tokenStorageService: TokenStorageService,
              private readonly modalCtrl: ModalController,
              private readonly toastController: ToastController) {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(
      this.tokenStorageService.getId().pipe(
        tap(userId => {
          if (userId) {
            this.userId = userId;
            this.loadRssFeeds();
          }
        })
      ).subscribe());
  }

  ionViewWillEnter() {
    this.loadRssFeeds();
  }

  loadRssFeeds() {
    if (this.userId) {
      this.rssFeeds$ = this.rssFeedsApi.getAllUserRssFeeds(this.userId);
    }
  }

  async openFeedModal(rssFeed: RssFeed = null) {
    const modal = await this.modalCtrl.create({
      component: FeedModalPage,
      cssClass: 'modal-css',
      backdropDismiss: false,
      componentProps: {
        title: rssFeed?.title || null,
        url: rssFeed?.url || null,
        id: rssFeed?.id || null,
        userId: rssFeed?.userId || this.userId
      }
    });

    await modal.present();

    modal.onDidDismiss().then((result) => {
      if (result.data) {
        this.saveRssFeed(result.data);
      }
    });
  }

  saveRssFeed(rssFeed: RssFeed) {
    let message: string;
    let color = 'success';
    this.isLoading = true;
    this.subscriptions.push(
      this.rssFeedsApi.createOrUpdateRssFeed(rssFeed).pipe(
        tap((res: any) => {
          message = res.body.message;
          this.loadRssFeeds();
        }),
        catchError((err) => {
          message = err.error.message;
          color = 'warning';
          return of();
        }),
        finalize(() => {
          this.isLoading = false;
          this.presentToast(message, color);
        })
      ).subscribe());
  }

  onAddFeed() {
    this.openFeedModal();
  }

  onUpdateFeed(rssFeed: RssFeed) {
    this.openFeedModal(rssFeed);
  }

  onDeleteFeed(id: number) {
    this.rssFeedsApi.deleteRssFeed(id).pipe(
      tap(_ => this.loadRssFeeds())
    ).subscribe();
  }

  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message,
      duration: 3000,
      color
    });
    await toast.present();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
