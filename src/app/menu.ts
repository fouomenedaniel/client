export interface MenuItem {
  title: string;
  url: string;
  icon: string;
  children?: MenuItem[];
  open?: boolean;
}

export const MENU_ITEMS: Array<MenuItem> = [
  {
    title: 'Accueil',
    url: 'home',
    icon: 'home-outline',
  },
  {
    title: 'Agenda',
    url: 'agenda',
    icon: 'calendar-outline'
  },
  {
    title: 'Croissance Enfant',
    url: null,
    icon: '',
    open: false,
    children: [
      {
        title: 'Evolution enfant',
        url: 'child-growth/growth-stage',
        icon: 'md-growth'
      },
      {
        title: 'Diversification alimentaire',
        url: 'child-growth/dietary-diversification',
        icon: 'md-diet'
      },
      {
        title: 'Vaccins',
        url: 'child-growth/vaccine',
        icon: 'md-vaccine'
      },
    ]
  },
  {
    title: 'Flux RSS',
    url: null,
    icon: 'logo-rss',
    open: false,
    children: [
      {
        title: 'Consulter flux RSS',
        url: 'rss-feeds/feed-items',
        icon: 'logo-rss'
      },
      {
        title: 'Gérer les flux RSS',
        url: 'rss-feeds/feed-sources',
        icon: 'logo-rss'
      },
      {
        title: 'Articles sauvegardés',
        url: 'rss-feeds/saved-feed-items',
        icon: 'logo-rss'
      }
    ]
  },
  {
    title: 'Profil',
    url: 'users/user-profil',
    icon: 'people-outline'
  }
];

export const MENU_ADMIN: Array<MenuItem> = [
  {
    title: 'Utilisateurs',
    url: 'users',
    icon: 'people-outline'
  }
];

export const MENU_LOGOUT: MenuItem = {
  title: 'Se déconnecter',
  url: 'logout',
  icon: 'log-out-outline'
};

export const MENU_LOGIN: MenuItem = {
  title: 'Se connecter',
  url: 'login',
  icon: 'log-in-outline'
};

