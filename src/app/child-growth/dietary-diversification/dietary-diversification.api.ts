import { Injectable } from '@angular/core';
import { CHILD_GROWTH_API_URL } from '../../constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Food } from '../../model/food';
import { createRequestOption } from '../../shared/requestUtils';
import { AbstractEntity } from '../../model/abstract-entity';

export interface FoodFilter {
  period: number;
  type: number;
}

@Injectable({
  providedIn: 'root'
})
export class DietaryDiversificationApi {
  rootUrl = `${CHILD_GROWTH_API_URL}/dietary-diversifications`;
  periodUrl = `${this.rootUrl}/periods`;
  typeUrl = `${this.rootUrl}/types`;
  suggestUrl = `${this.rootUrl}/suggest`;

  constructor(protected readonly http: HttpClient) {}

  getFoods(req: FoodFilter): Observable<Food[]> {
    const options = createRequestOption(req);
    return this.http.get<Food[]>(this.rootUrl, {params: options});
  }

  getFood(id: number): Observable<Food> {
    return this.http.get<Food>(`${this.rootUrl}/${id}`);
  }

  suggestFood(search: string): Observable<Food[]> {
    return this.http.get<Food[]>(this.suggestUrl, {params: {name: search}});
  }

  getPeriods(): Observable<AbstractEntity[]> {
    return this.http.get<AbstractEntity[]>(this.periodUrl);
  }

  getTypes(): Observable<AbstractEntity[]> {
    return this.http.get<AbstractEntity[]>(this.typeUrl);
  }
}
