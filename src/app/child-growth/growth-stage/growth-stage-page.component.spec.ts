import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GrowthStagePage } from './growth-stage-page.component';

describe('ChildGrowthPage', () => {
  let component: GrowthStagePage;
  let fixture: ComponentFixture<GrowthStagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrowthStagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GrowthStagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
