import { Component, OnInit } from '@angular/core';
import { GrowthStageApi, GrowthStageFilters } from './growth-stage.api';
import { Observable } from 'rxjs';
import { GrowthStage } from '../../model/growth-stage';
import { Period } from '../../model/period';
import { GrowthStageType } from '../../model/growth-stage-type';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-child-growth',
  templateUrl: './growth-stage-page.component.html',
  styleUrls: ['./growth-stage-page.component.scss'],
})
export class GrowthStagePage implements OnInit {

  growthStages$: Observable<GrowthStage[]>;
  growthStagesPeriods$: Observable<Period[]>;
  growthStagesTypes$: Observable<GrowthStageType[]>;
  growthStageFilterForm: FormGroup;

  constructor(private readonly growthStageApi: GrowthStageApi,
              private readonly formBuilder: FormBuilder) {}

  ngOnInit() {
    this.growthStages$ = this.growthStageApi.getGrowthStages();
    this.growthStagesPeriods$ = this.growthStageApi.getGrowthStagePeriods();
    this.growthStagesTypes$ = this.growthStageApi.getGrowthStageTypes();
    this.initForm();
  }

  initForm() {
    this.growthStageFilterForm = this.formBuilder.group({
      periods: null,
      types: null
    });
  }

  onFilterChange() {
    const periods = this.growthStageFilterForm.controls.periods.value;
    const types = this.growthStageFilterForm.controls.types.value;
    const filters: GrowthStageFilters = {
      periods: periods != null && periods.length > 0 ? periods : null,
      types: types != null && types.length > 0 ? types : null
    };
    this.growthStages$ = this.growthStageApi.getGrowthStages(filters);
  }


}
