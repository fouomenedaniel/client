import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ChildGrowthRoutingModule } from './child-growth-routing.module';
import { GrowthStagePage } from './growth-stage/growth-stage-page.component';
import { SharedModule } from '../shared/shared.module';
import { VaccinePage } from './vaccine/vaccine.page';
import { DietaryDiversificationPage } from './dietary-diversification/dietary-diversification.page';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChildGrowthRoutingModule,
    SharedModule,
    IonicSelectableModule
  ],
  declarations: [
    GrowthStagePage,
    VaccinePage,
    DietaryDiversificationPage
  ]
})
export class ChildGrowthPageModule {}
