import { Injectable } from '@angular/core';
import { AGENDA_API_URL, CHILD_GROWTH_API_URL } from '../constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Vaccine } from '../model/vaccine';
import { createRequestOption } from '../shared/requestUtils';
import { Appointment } from '../model/appointment';
import { FeedItem } from '../model/feed-item';
import { RssFeedsApi } from '../rss-feeds/rss-feeds.api';
import { Child } from '../model/child';
import { UserApi } from '../shared/user.api';
import { map } from 'rxjs/operators';
import { WidgetFood } from '../model/widget-food';
import { WidgetGrowthStage } from '../model/widget-growth-stage';

@Injectable({
  providedIn: 'root'
})
export class WidgetApi {

  agendaResourceUrl = `${AGENDA_API_URL}/appointments/next`;
  vaccineResourceUrl = `${CHILD_GROWTH_API_URL}/vaccines/children`;
  foodResourceUrl = `${CHILD_GROWTH_API_URL}/dietary-diversifications/children`;
  growthStageResourceUrl = `${CHILD_GROWTH_API_URL}/growth-stages/children`;

  constructor(private readonly http: HttpClient,
              private readonly rssFeedApi: RssFeedsApi,
              private readonly userApi: UserApi) { }

  getUserChildren(userId: string): Observable<Child[]> {
    return this.userApi.getUserByExternalId(userId).pipe(
      map(user => user.children)
    );
  }

  getChildNextVaccines(birthDate: string): Observable<Vaccine[]> {
    const options = createRequestOption({birthDate});
    return this.http.get<Vaccine[]>(this.vaccineResourceUrl, {params: options});
  }

  getUserNextAppointments(userId: string): Observable<Appointment[]> {
    const options = createRequestOption({userId});
    return this.http.get<Appointment[]>(this.agendaResourceUrl, {params: options});
  }

  getParentsNews(): Observable<FeedItem[]> {
    return this.rssFeedApi.readFeed(1, null);
  }

  getChildFood(birthDate: string): Observable<WidgetFood[]> {
    const options = createRequestOption({birthDate});
    return this.http.get<WidgetFood[]>(this.foodResourceUrl, {params: options});
  }

  getChildGrowthStages(birthDate: string): Observable<WidgetGrowthStage[]> {
    const options = createRequestOption({birthDate});
    return this.http.get<WidgetGrowthStage[]>(this.growthStageResourceUrl, {params: options});
  }
}
