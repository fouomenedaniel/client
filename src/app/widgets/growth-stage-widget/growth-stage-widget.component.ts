import { Component, OnDestroy, OnInit } from '@angular/core';
import { Child } from '../../model/child';
import { Observable, Subscription } from 'rxjs';
import { WidgetGrowthStage } from '../../model/widget-growth-stage';
import { WidgetApi } from '../widget.api';
import { TokenStorageService } from '../../shared/token-storage.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-growth-stage-widget',
  templateUrl: './growth-stage-widget.component.html',
  styleUrls: ['./growth-stage-widget.component.scss'],
})
export class GrowthStageWidgetComponent implements OnInit, OnDestroy {

  userId: string;
  children: Child[];
  growthStages$: Observable<WidgetGrowthStage[]>;
  subscriptions: Subscription[];

  constructor(private readonly widgetApi: WidgetApi,
              private readonly tokenStorageService: TokenStorageService) {
    this.children = [];
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(
      this.tokenStorageService.getId().pipe(
        tap(userId =>
          this.subscriptions.push(
            this.widgetApi.getUserChildren(userId).pipe(
              tap(children => this.children = children),
              tap(_ => {
                if (this.children.length > 0) {
                  this.loadChildGrowthStages(this.children[0].birthDate);
                }
              })
            ).subscribe())
        )
      ).subscribe()
    );
  }

  loadChildGrowthStages(birthDate: string) {
    this.growthStages$ = this.widgetApi.getChildGrowthStages(birthDate);
  }

  onChildChange(event: CustomEvent) {
    this.loadChildGrowthStages(event.detail.value.birthDate);
  }

  compareWith(o1: Child, o2: Child) {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
