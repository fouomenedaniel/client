import { Component, OnDestroy, OnInit } from '@angular/core';
import { Child } from '../../model/child';
import { Observable, Subscription } from 'rxjs';
import { WidgetApi } from '../widget.api';
import { TokenStorageService } from '../../shared/token-storage.service';
import { tap } from 'rxjs/operators';
import { Vaccine } from '../../model/vaccine';

@Component({
  selector: 'app-vaccine-widget',
  templateUrl: './vaccine-widget.component.html',
  styleUrls: ['./vaccine-widget.component.scss'],
})
export class VaccineWidgetComponent implements OnInit, OnDestroy {

  userId: string;
  children: Child[];
  vaccines$: Observable<Vaccine[]>;
  subscriptions: Subscription[];

  constructor(private readonly widgetApi: WidgetApi,
              private readonly tokenStorageService: TokenStorageService) {
    this.children = [];
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(
      this.tokenStorageService.getId().pipe(
        tap(userId =>
          this.subscriptions.push(
            this.widgetApi.getUserChildren(userId).pipe(
              tap(children => this.children = children),
              tap(_ => {
                if (this.children.length > 0) {
                  this.loadChildVaccine(this.children[0].birthDate);
                }
              })
            ).subscribe())
        )
      ).subscribe()
    );
  }

  loadChildVaccine(birthDate: string) {
    this.vaccines$ = this.widgetApi.getChildNextVaccines(birthDate);
  }

  onChildChange(event: CustomEvent) {
    this.loadChildVaccine(event.detail.value.birthDate);
  }

  compareWith(o1: Child, o2: Child) {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
