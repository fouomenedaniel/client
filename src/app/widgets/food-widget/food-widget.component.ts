import { Component, OnDestroy, OnInit } from '@angular/core';
import { Child } from '../../model/child';
import { Observable, Subscription } from 'rxjs';
import { WidgetFood } from '../../model/widget-food';
import { WidgetApi } from '../widget.api';
import { TokenStorageService } from '../../shared/token-storage.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-food-widget',
  templateUrl: './food-widget.component.html',
  styleUrls: ['./food-widget.component.scss'],
})
export class FoodWidgetComponent implements OnInit, OnDestroy {

  userId: string;
  children: Child[];
  food$: Observable<WidgetFood[]>;
  subscriptions: Subscription[];

  constructor(private readonly widgetApi: WidgetApi,
              private readonly tokenStorageService: TokenStorageService) {
    this.children = [];
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(
      this.tokenStorageService.getId().pipe(
        tap(userId =>
        this.subscriptions.push(
          this.widgetApi.getUserChildren(userId).pipe(
            tap(children => this.children = children),
            tap(_ => {
              if (this.children.length > 0) {
                this.loadChildFood(this.children[0].birthDate);
              }
            })
          ).subscribe())
        )
      ).subscribe()
    );
  }

  loadChildFood(birthDate: string) {
    this.food$ = this.widgetApi.getChildFood(birthDate);
  }

  onChildChange(event: CustomEvent) {
    this.loadChildFood(event.detail.value.birthDate);
  }

  compareWith(o1: Child, o2: Child) {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
