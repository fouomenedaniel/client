import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { HomePageRoutingModule } from './home-routing.module';
import { AgendaWidgetComponent } from '../widgets/agenda-widget/agenda-widget.component';
import { SharedModule } from '../shared/shared.module';
import { RssFeedWidgetComponent } from '../widgets/rss-feed-widget/rss-feed-widget.component';
import { VaccineWidgetComponent } from '../widgets/vaccine-widget/vaccine-widget.component';
import { UsefulLinksWidgetComponent } from '../widgets/useful-links-widget/useful-links-widget.component';
import { FoodWidgetComponent } from '../widgets/food-widget/food-widget.component';
import { GrowthStageWidgetComponent } from '../widgets/growth-stage-widget/growth-stage-widget.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    SharedModule
  ],
  declarations: [
    HomePage,
    AgendaWidgetComponent,
    RssFeedWidgetComponent,
    VaccineWidgetComponent,
    UsefulLinksWidgetComponent,
    FoodWidgetComponent,
    GrowthStageWidgetComponent
  ]
})
export class HomePageModule {}
