import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment } from '@angular/router';
import { TokenStorageService } from './shared/token-storage.service';

@Injectable()
export class AdminGuardService implements CanActivate, CanLoad {

  constructor(private readonly tokenStorageService: TokenStorageService,
              private readonly router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.isAdmin()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }

  canLoad(route: Route, segments: UrlSegment[]): boolean {
    if (!this.isAdmin()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }

  isAdmin(): boolean {
    return this.tokenStorageService.roleSubject.value === 'ADMIN';
  }

}
