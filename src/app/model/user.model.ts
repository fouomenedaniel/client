import { UserRole } from './user-role.enum';
import { Child } from './child';

export class User {
    id: number;
    username: string;
    password: string;
    email: string;
    role: UserRole;
    children: Child[];
    externalId?: string;

    constructor(data: Partial<User>) {
        Object.assign(this, data);
        if (data.children) {
            this.children = data.children.map(child => new Child(child));
        }
    }

}
