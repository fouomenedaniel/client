import { UserWithoutPassword } from './user-without-password.model';

export class UserWithoutPasswordPage {

    number: number;
    size: number;
    totalPages: number;
    last: boolean;
    totalElement: number;
    content: Array<UserWithoutPassword> = new Array<UserWithoutPassword>();
}
