export class SavedFeedItem {
  id: number;
  userId: string;
  title: string;
  url: string;

  constructor(userId: string, title: string, url: string) {
    this.userId = userId;
    this.title = title;
    this.url = url;
  }
}
