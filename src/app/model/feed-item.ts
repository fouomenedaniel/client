import { FeedItemImage } from './feed-item-image';

export class FeedItem {
  title: string;
  url: string;
  description: string;
  image: FeedItemImage;
}
