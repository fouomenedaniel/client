import { AbstractEntity } from './abstract-entity';

export class Food {
  id: number;
  name: string;
  type: AbstractEntity;
  comment: string;
  period: AbstractEntity;
}
