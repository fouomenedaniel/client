import { Period } from './period';
import { GrowthStageType } from './growth-stage-type';

export class GrowthStage {
  id: number;
  period: Period;
  type: GrowthStageType;
  description: string;

  constructor() {}
}
