import { UserRole } from './user-role.enum';

export class UserWithoutPassword {
    id: number;
    username: string;
    email: string;
    role: UserRole;
    externalId: string;

    constructor() {}

    userWithoutPasswordFromPartial(data: Partial<UserWithoutPassword>) {
        Object.assign(this, data);
    }
}
