import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'foodColor'
})
export class FoodColorPipe implements PipeTransform {

  transform(value: number): string {
    if (value) {
      switch (value) {
        case 1:
          return 'success';
        case 2:
          return 'primary';
        case 3:
          return 'tertiary';
        case 4:
          return 'pink';
        case 5:
          return 'medium';
        case 6:
          return 'light';
        case 7:
          return 'warning';
        case 8:
          return 'danger';
        case 9:
          return 'yellow';
        case 10:
          return 'secondary';
      }
    }
    return null;
  }

}
