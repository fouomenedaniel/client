import { AppointmentType } from '../model/appointment-type.enum';

export function getAppointmentTypeStringValue(type: AppointmentType): string {
  switch (type) {
    case AppointmentType.DOCTOR:
      return 'Médecin';
    case AppointmentType.VACCINE:
      return 'Vaccin';
    case AppointmentType.PHARMACY:
      return 'Pharmacie';
    case AppointmentType.PEDIATRIC_NURSE:
      return 'Pédiatre';
    case AppointmentType.COMPULSORY_MEDICAL_EXAMINATION:
      return 'Examen médical obligatoire';
    case AppointmentType.OTHER:
      return 'Autre';
  }
}

export function getAppointmentTypeFromStringValue(type: string): AppointmentType {
  switch (type) {
    case 'Médecin':
      return AppointmentType.DOCTOR;
    case 'Vaccin':
      return AppointmentType.VACCINE;
    case 'Pharmacie':
      return AppointmentType.PHARMACY;
    case 'Pédiatre':
      return AppointmentType.PEDIATRIC_NURSE;
    case 'Examen médical obligatoire':
      return AppointmentType.COMPULSORY_MEDICAL_EXAMINATION;
    case 'Autre':
      return AppointmentType.OTHER;
  }
}
