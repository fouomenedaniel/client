import { Injectable } from '@angular/core';
import { AuthResponse } from '../auth/auth-response';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, from, Observable } from 'rxjs';

export const TOKEN_KEY = 'ACCESS_TOKEN';
export const EXPIRATION_KEY = 'EXPIRES_IN';
export const ROLE_KEY = 'ROLE';
export const ID_KEY = 'ID';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  roleSubject = new BehaviorSubject(null);
  authenticadedSubject = new BehaviorSubject(false);

  constructor(private readonly storage: Storage) {}

  storeAuthInformations(authResponse: AuthResponse) {
    this.storage.set(TOKEN_KEY, authResponse.token);
    this.storage.set(EXPIRATION_KEY, authResponse.expiresIn);
    this.storage.set(ROLE_KEY, authResponse.roles[0]);
    this.storage.set(ID_KEY, authResponse.id);
    this.roleSubject.next(authResponse.roles[0]);
    this.authenticadedSubject.next(true);
  }

  removeAuthInformations() {
    this.storage.remove(TOKEN_KEY);
    this.storage.remove(EXPIRATION_KEY);
    this.storage.remove(ROLE_KEY);
    this.storage.remove(ID_KEY);
    this.roleSubject.next(null);
    this.authenticadedSubject.next(false);
  }

  getToken(): Observable<string> {
    return from(this.storage.get(TOKEN_KEY));
  }

  getId(): Observable<string> {
    return from(this.storage.get(ID_KEY));
  }

}
