import { Component, OnDestroy, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MENU_ADMIN, MENU_ITEMS, MENU_LOGIN, MENU_LOGOUT, MenuItem } from './menu';
import { TokenStorageService } from './shared/token-storage.service';
import { tap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  menu: MenuItem[];
  menuAdmin: MenuItem[];
  menuLogout: MenuItem;
  menuLogin: MenuItem;
  isAdmin: boolean;
  isAuthenticated: boolean;
  subscriptions: Subscription[];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private readonly tokenStorageService: TokenStorageService,
    private readonly authService: AuthService
  ) {
    this.menu = MENU_ITEMS;
    this.menuAdmin = MENU_ADMIN;
    this.menuLogout = MENU_LOGOUT;
    this.menuLogin = MENU_LOGIN;
    this.subscriptions = [];
    this.initializeApp();
  }

  ngOnInit(): void {
    this.subscriptions.push(
      this.tokenStorageService.roleSubject.pipe(
        tap(value => this.isAdmin = value === 'ADMIN')
      ).subscribe()
    );
    this.subscriptions.push(
      this.tokenStorageService.authenticadedSubject.pipe(
        tap(value => this.isAuthenticated = value)
      ).subscribe()
    );
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => element.unsubscribe());
  }

  onLogout() {
    this.authService.logout();
  }
}
