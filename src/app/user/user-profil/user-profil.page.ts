import { Component, OnDestroy, OnInit } from '@angular/core';
import { TokenStorageService } from '../../shared/token-storage.service';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserApi } from '../../shared/user.api';
import { User } from '../../model/user.model';
import { ModalController } from '@ionic/angular';
import { Child } from '../../model/child';
import { ChildApi } from '../child.api';
import { ChildSaveModalPage } from '../child-save-modal/child-save-modal.page';

@Component({
  selector: 'app-user-profil',
  templateUrl: './user-profil.page.html',
  styleUrls: ['./user-profil.page.scss'],
})
export class UserProfilPage implements OnInit, OnDestroy {

  user$: Observable<User>;
  subscriptions: Subscription[];
  userId: string;

  constructor(private readonly tokenStorageService: TokenStorageService,
              private readonly userApi: UserApi,
              private readonly childApi: ChildApi,
              private readonly modalCtrl: ModalController) {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(
      this.tokenStorageService.getId().pipe(
        tap(userId => {
          if (userId) {
            this.userId = userId;
            this.loadUser();
          }
        })
      ).subscribe());
  }

  ionViewWillEnter() {
    this.loadUser();
  }

  onAddChild() {
    this.openChildSaveModal();
  }

  onUpdateChild(child: Child) {
    this.openChildSaveModal(child);
  }

  onDeleteChild(id: number) {
    this.subscriptions.push(
      this.childApi.deleteChild(id).pipe(
        tap(_ => this.loadUser())
      ).subscribe()
    );
  }

  async openChildSaveModal(child: Child = null) {
    const modal = await this.modalCtrl.create({
      component: ChildSaveModalPage,
      cssClass: 'modal-css',
      backdropDismiss: false,
      componentProps: { childInput : child ?? null }
    });

    await modal.present();

    modal.onDidDismiss().then((result) => {
      if (result.data) {
        this.saveChild(result.data.child);
      }
    });
  }

  saveChild(child: Child) {
    child.userId = child.userId || this.userId;
    child.birthDate = child.birthDate.substring(0, 10);
    this.subscriptions.push(
      this.childApi.saveChild(child).pipe(
        tap(_ => this.loadUser())
      ).subscribe()
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }

  private loadUser() {
    this.user$ = this.userApi.getUserByExternalId(this.userId);
  }

}
