import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserProfilPage } from './user-profil/user-profil.page';
import { ChildSaveModalPage } from './child-save-modal/child-save-modal.page';
import { UserListPage } from './user-list/user-list.page';
import { UserSavePage } from './user-save/user-save.page';
import { AdminGuardService } from '../admin-guard.service';

const routes: Routes = [
  {
    path: 'user-list',
    canActivate: [AdminGuardService],
    component: UserListPage
  },
  {
    path: 'user-save',
    canActivate: [AdminGuardService],
    component: UserSavePage
  },
  {
    path: 'user-profil',
    component: UserProfilPage
  },
  {
    path: 'child-save-modal',
    component: ChildSaveModalPage
  },
  {
    path: '',
    component: UserListPage
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserPageRoutingModule {}
