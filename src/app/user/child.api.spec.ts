import { TestBed } from '@angular/core/testing';

import { ChildApi } from './child.api';

describe('ChildApi', () => {
  let service: ChildApi;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChildApi);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
