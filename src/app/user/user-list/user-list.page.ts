import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserWithoutPassword } from '../../model/user-without-password.model';
import { UserApi } from '../../shared/user.api';
import { map } from 'rxjs/operators';
import { UserService } from '../../shared/user.service';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.page.html',
  styleUrls: ['./user-list.page.scss'],
})
export class UserListPage implements OnDestroy {

  page = 1;
  users: UserWithoutPassword[];
  subscriptions: Subscription[] = [];

  constructor(private readonly userApi: UserApi,
              private readonly userService: UserService,
              private readonly router: Router,
              public readonly actionSheetController: ActionSheetController) { }

  ionViewWillEnter() {
    this.loadUsers();
  }

  async actionSheet(user) {
    const actionSheet = await this.actionSheetController.create({
      header: user.username,
      buttons: [{
        text: 'Supprimer',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.subscriptions.push(
              this.userApi.deleteUser(user.id)
                  .pipe(
                      map(() => this.loadUsers())
                  ).subscribe()
          );
        }
      }, {
        text: 'Modifier',
        icon: 'create',
        handler: () => {
          this.updateUser(user);
        }
      }, {
        text: 'Annuler',
        icon: 'close',
        role: 'cancel'
      }]
    });
    await actionSheet.present();
  }

  loadUsers() {
    this.subscriptions.push(this.userApi.getUsers({
      unpaged: true
    })
        .pipe(
            map(element => this.users = element.content)
        ).subscribe());
  }

  updateUser(user: UserWithoutPassword) {
    this.userService.setUser(user);
    this.userService.setSaveMethod('update');
    this.router.navigate(['/user-save']);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => element.unsubscribe());
  }

}
