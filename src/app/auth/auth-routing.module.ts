import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginPage } from './login/login.page';
import { RegisterPage } from './register/register.page';
import { LogoutPage } from './logout/logout.page';
import { AuthGuardService } from '../auth-guard.service';

const routes: Routes = [
  {
    path: 'login',
    component: LoginPage
  },
  {
    path: 'register',
    component: RegisterPage
  },
  {
    path: 'logout',
    component: LogoutPage,
    canActivate: [AuthGuardService]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AuthRoutingModule {}
