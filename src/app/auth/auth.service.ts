import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { AuthApi } from './auth.api';
import { User } from '../model/user.model';
import { tap } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { TokenStorageService } from '../shared/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {

  subscriptions: Subscription[] = [];

  constructor(private readonly authApi: AuthApi,
              private readonly tokenStorageService: TokenStorageService,
              private readonly router: Router) { }

  login(user: User) {
    this.subscriptions.push(
        this.authApi.login(user).pipe(
          tap(async (res: any) => {
            if (res.token) {
              await this.tokenStorageService.storeAuthInformations(res);
              await this.router.navigate(['home']);
            } else {
              console.error(res.error);
            }
          })
        ).subscribe()
    );
  }

  register(user: User) {
    this.subscriptions.push(
        this.authApi.register(user).pipe(
            tap(async (res: HttpResponse<any> | HttpErrorResponse) => {
              if (res.status === 200) {
               await this.router.navigate(['/login']);
              }
            })
        ).subscribe()
    );
  }

  async logout() {
    await this.tokenStorageService.removeAuthInformations();
    await this.router.navigate(['/login']);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => element.unsubscribe());
  }
}
