import { Injectable } from '@angular/core';
import { USER_API_URL } from '../constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { User } from '../model/user.model';
import { Observable } from 'rxjs';
import { AuthResponse } from './auth-response';

@Injectable({
  providedIn: 'root'
})
export class AuthApi {

  resourceUrl = `${USER_API_URL}/auth`;

  constructor(protected readonly http: HttpClient) { }

  login(user: User): Observable<AuthResponse> {
    return this.http.post<AuthResponse>(`${this.resourceUrl}/signin`, user);
  }

  register(user: User): Observable<HttpResponse<any>> {
    return this.http.post<HttpResponse<any>>(`${this.resourceUrl}/signup`, user, {observe: 'response'});
  }

}
